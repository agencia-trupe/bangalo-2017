#!/bin/bash

export PATH=$HOME/local/node/bin:$PATH

for i in *.less ; do
	c=$(echo $i|sed 's/less/css/g')
	lessc $i > $c -x
done

case $1 in
	-d) rm -rf *.less;;
esac
