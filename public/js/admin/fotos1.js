/*global $ */
$(document).ready(function () {
    $('#file_upload').fileUploadUI({
        uploadTable: $('#file_upload_list'),
        downloadTable: $('.list_fotos'),
        buildUploadRow: function (files, index) {
            //alert(files[index].size);
			//return $('<tr><td class="filename">' + files[index].name + '<\/td>' +
            return $('<tr>'+
					//'<td class="filename file_upload_preview"><\/td>' +
					'<td class="file_upload_progress"><div></div></td>' +
					'<td class="filesize">'+Files.formatBytes(files[index].size)+'</td>' +
                    '<td class="file_upload_cancel">' +
                    '<button class="ui-state-default ui-corner-all" title="Cancel">' +
                    '<span class="ui-icon ui-icon-cancel">Cancel<\/span>' +
                    '<\/button>' +
					'<\/td>' +
					//'<td class="file_delete">' +
					//'<button title="Delete" class="ui-state-default ui-corner-all file-delete">' +
					//'<span class="ui-icon ui-icon-trash"><\/span>' +
					//'<\/button>' +
					//'<\/td>' +
					'<\/tr>');
        },
        buildDownloadRow: function (file) {
            //return $('<tr><td>' + file.name + '<\/td><\/tr>');
            if(file.error){
				return $('<tr><td><span>' + file.error + '<\/span><\/td><\/tr>');
			} else {
				return $('<li><a href=\'' + URL+'/img/'+DIR+":"+file.name + '\' rel=\'gallery_group\'>'+
						 '<img src=\'' + URL+'/img/'+DIR+":"+file.name + '\'><\/a>'+ // 3 fotos
						 // '<img src=\'' + URL+'/img/'+DIR+":"+file.name + '?w=90&h=90\'><\/a>'+ // 3 fotos
						 //'<img src=\'' + URL+'/img/'+DIR+":"+file.name + '?w=65&h=65\'><\/a>'+ // 4 fotos
						 ((typeof(CORES_FOTOS)!=='undefined')?_combo('name="foto_cor'+file.id+'" id="foto_cor_'+file.id+'" class="foto_cor txt"',CORES_FOTOS):'')+
						 '<button title="Delete" class="ui-state-default ui-corner-all file-delete">' +
						 '<span class="ui-icon ui-icon-trash"><\/span>' +
						 '<\/button>' +
						 ((allow_photos) ? '<button title="Inserir esta imagem no texto" class="ui-state-default ui-corner-all file-insert">'+
							'<span class="ui-icon ui-icon-arrowthick-1-w"></span>'+
							'</button>' : '') +
						 '<input type="hidden" name="foto_id[]" value="'+file.id+'" class="foto_id" />' +
						 '<\/li>');
			}
        },
		beforeSend: function(event, files, index, xhr, handler, callBack){
			var patt_img  = /.*(gif|jpeg|png|jpg)$/i,///(jpeg|jpg|png|gif|bmp)/gi,
				file_name = files[index].name,
				max_size  = (MAX_SIZE)  ? MAX_SIZE  : 2097152,
				max_files = (MAX_FOTOS) ? MAX_FOTOS : 0,
				file_count= $(".list_fotos li").size(),
				max_count = (max_files>0) ? file_count < max_files : true,
				msg       = "Erro ao enviar imagem";
			//console.log(file_name+","+Files.getExt(file_name)+","+patt_img.test(Files.getExt(file_name)));
			//console.log(files[index].size < max_size);
			//console.log(max_count);
			//return false;
			if(patt_img.test(Files.getExt(file_name)) && files[index].size < max_size && max_count){
				callBack();
				return true;
			}
			
			/*switch(false){
				case patt_img.test(Files.getExt(file_name)):
					msg = "Imagem inválida"; break;
				case files[index].size < max_size:
					msg = "Tamanho máx. permitido: "+Files.formatBytes(max_size); break;
				case max_count:
					msg = "Você pode enviar no máximo "+max_files+" fotos"; break;
					//msg = "Erro ao enviar imagem"; break;
				default:
					callBack();
					return true;
			}*/ 
			msg = !patt_img.test(Files.getExt(file_name)) ? "Imagem inválida" : msg;
			msg = files[index].size >= max_size ? "Tamanho máx. permitido: "+Files.formatBytes(max_size) : msg;
			msg = !max_count ? "Você pode enviar no máximo "+max_files+" fotos" : msg;
			
			handler.cancelUpload(event, files, index, xhr, handler);
			handler.addNode($('#file_upload_list'),
				$('<tr class="error"><td colspan="3">'+msg+' ('+file_name+')</td></tr>')
			);
			//callBack();
		},
		onComplete: function (event, files, index, xhr, handler) {
			// var json = handler.response;
			Gallery.show();
		}
    });
    
    $("button.file-delete").live('click',function(){
        var row = $(this).parents("li");
		Files.del(row);
    });
	$("button.file-insert").live('click',function(){
        var row = $(this).parents("li");
		Files.insert(row);
    });
    $("button.save-all").live('click',function(){
        var row = $(this).parents("li");
		Files.saveAll(row);
    });
    
    $("#files tr").mouseover(function(){
        $(this).addClass("hover");
    }).mouseout(function(){
        $(this).removeClass("hover");
    });
	
	$("#file_upload_list tr.error").live('click',function(){
		$this = $(this);
		
		$this.fadeOut("fast",function(){
			$this.remove();
		})
	});

	// links
	_prevalueLinks();

    $('input.foto_link').live('keyup',function(e){
        if(e.which==13) Files.saveAll($(this).parents('li'));
    });
});

function _prevalueLinks(){
	$('input.foto_link').each(function(i,v){
        var $t = $(this);
        if($.trim($t.val())=='') $t.prevalue('URL...');
    });
}

Files = {
    formatBytes: function(size){
        var units = new Array(' B', ' KB', ' MB', ' GB', ' TB');
        for (var i = 0; size >= 1024 && i < 4; i++) size /= 1024;
        return Math.round(size)+units[i];
    },

    saveAll: function(row){
    	var data         = {},
    		inputs       = row.find('input'),
    		selects      = row.find('select'),
    		url          = URL+GLOBAL_URL+'save-all.json',
            status       = row.find('.foto_status');
    		invalid_vals = [
    						'',
    						'...',
			    			'título...',
			    			'descrição...',
			    			'selecione...',
			    			'url...'
			    		];

    	inputs.each(function(){
    		var $t = $(this);
    		if($t.data('name') && $.inArray($.trim($t.val().toLowerCase()),invalid_vals)==-1)
    			data[$t.data('name')] = $.trim($t.val());
    	});

    	selects.each(function(){
    		var $t = $(this);
    		if($t.data('name') && $.inArray($t.val().toLowerCase(),invalid_vals)==-1)
    			data[$t.data('name')] = $t.val();
    	});

        status.html('Aguarde...').show();
        
        $.post(url,data,function(json){
            if(json.error){
                alert(json.error);
                status.html('').hide();
                return false;
            }
            
            status.html(json.msg||'Concluído').delay(3000).fadeOut('slow');
        },'json');
    },
    
    del: function(row,dir){
        dir = dir || false;
        var url = URL+"/admin/"+DIR+"/fotos-del.json?file="+row.find("input.foto_id").val();
        
        if(confirm("Deseja deletar a foto selecionada?")){
            $.getJSON(url,function(data){
                if(data.error){
                    alert(data.error);
                } else {
                    row.fadeOut("slow",function(){ $(this).remove(); });
                }
            });
        }
    },
	
	insert: function(row){
		var path = row.find('a').attr('href');
		$('textarea.wysiwyg').wysiwyg('insertImage', path);
	},
	
	getExt: function(name){
		return name.split(".").reverse()[0];
	}
}


function _combo(attrs,values){
	var combo = '<select '+attrs+'>',v = values || [];
	for(i in v) combo+= '<option value="'+i+'">'+v[i]+'</option>';
	combo+= '</select>';
	return combo;
}