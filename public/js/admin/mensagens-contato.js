var MAX_FOTOS = 0, MAX_SIZE = 5242880, allow_photos;
var lastChecked = null;

$(document).ready(function(){
    $('textarea.wysiwyg').wysiwyg({
        css:{fontFamily:'Arial,sans-serif',fontSize:'12px',color:'#3C3C3C',margin:0,padding:0},
        visible:"h3,bold,italic,underline,separator00,insertUnorderedList,insertOrderedList,separator01,"+
                "createLink"+(allow_photos?",insertImage":"")+",separator02,undo,redo,separator03,removeFormat"
    });
    $('div.wysiwyg,div.wysiwyg iframe').css({'width':'495px'});
    $('div.wysiwyg').css({'margin-left':'110px'});
    $('div.wysiwyg div').remove();
    
    $("#frm-"+DIR).submit(function(e){
        if($.trim($("#titulo").val()).length == 0){
            $("#titulo").focus();
            Msg.add("Preencha todos os campos corretamente.","erro").show(5000);
            return false;
        }
        
        return true;
    });

    $("#search-by").change(function(){
        if($(this).val() == "mensagem_id"){
            var $combo = $('<select></select>');
            $combo.attr({
                'name'  : 'search-txt',
                'id'    : 'search-txt',
                'style' : 'width: 180px;'
            }).addClass('txt');
            
            if(mensagens){
                for(i in mensagens){
                    $combo.append('<option value="'+i+'">'+mensagens[i]+'</option>');
                };
            }
            
            $("#search-txt").replaceWith($combo);
        } else if($("#search-txt").attr("type").indexOf('select') != -1){
            var $combo = $('<input/>');
            $combo.attr('name','search-txt').attr('id','search-txt').addClass('txt');
            $("#search-txt").replaceWith($combo);
        }
    });
    
    $("#submit").click(function(e){
        $("#frm-"+DIR).submit(e);
    });

    // funções de check para exclusão ------------------------//
    var $chkboxes = $('input.check-del');

    // seleciona todos os checkbox
    $('input[name=check_del_all]').change(function(e){
        var $this = $(this),
            checked = $this.attr('checked');
        
        _checkAll(checked);
        _checkDelSelected();
    });

    // seleção de um checkbox
    $chkboxes.change(function(e){
        var $this = $(this),
            checked = $this.attr('checked');

        _checkDelSelected();
    });

    // envio de form excluir selecionados
    $('.form-del-select').submit(function(e){
        if(!confirm('Deseja deletar os itens selecionados?')){
            e.preventDefault();
            return false;
        }
    });

    // marcando checks com shift
    $chkboxes.click(function(e) {
        if(!lastChecked) {
            lastChecked = this;
            return;
        }

        if(e.shiftKey) {
            var start = $chkboxes.index(this),
                end = $chkboxes.index(lastChecked);

            $chkboxes.slice(Math.min(start,end), Math.max(start,end)+ 1).attr('checked', lastChecked.checked);
        }

        lastChecked = this;
    });
});

function _checkAll(checked) {
    $('.check-del').each(function(i,elm){
        elm.checked = checked;
    });
}

function _checkDelSelected() {
    var checks = 0, ids = [];
    $('.check-del').each(function(i,elm){
        if(elm.checked) checks++;
        if(elm.checked) ids.push(elm.value);
    });
    $('#delid').val(ids.join(','));
    
    if(checks > 0) 
        $('.form-del-select .submit')
            .attr('disabled',false)
            .removeClass('disabled');
    else 
        $('.form-del-select .submit')
            .attr('disabled',true)
            .addClass('disabled');
}