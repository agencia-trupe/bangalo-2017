$(document).ready(function(){
    // enviando contato
    if(CONTROLLER=='contato'&&ACTION=='index'){
    _prevalues();
    
    $('input#telefone').focus(function(){
        $(this).removeClass('pre');
    }).blur(function(){
        var $t = $(this);
        if($t.val() == ''){
            $t.addClass('pre');
        }
    });
    
    // enviando contato
    $('a#submit').click(function(){
        var $status = $('p.status').removeClass('error');
        $status.html('Enviando...');
        
        head.js(JS_PATH+'/is.simple.validate.js',function(){
            if($('form.contato').isSimpleValidate({pre:true})){
                var url  = URL+'/'+CONTROLLER+'/enviar.json',
                    data = $('form.contato').serialize();
                
                $.post(url,data,function(json){
                    if(json.error){
                        $status.addClass('error').html(json.error);
                        return false;
                    }
                    
                    // $status.html(json.msg);
                    $status.html(json.msg);
                    _resetForm('form.contato');
                    /*Shop.messageBox('<p class="shop-message-title contato">'+json.msg+'</p>'+
                                    '<p class="shop-message-buttons">'+
                                    '<a href="#" class="shop-close-message-box fechar-contato single">Fechar</a>'+
                                    '</p>');*/
                },'json');
            } else {
                $status.addClass('error').html('* Preencha todos os campos');
            }
        });
    });
    } // if ACTION index

    // trabalhe-conosco
    $.getCss(JS_PATH+'/fileinput/fileinput.css');
    head.js(JS_PATH+'/fileinput/jquery-ui-1.8.9.custom.min.js',JS_PATH+'/fileinput/jquery.fileinput.min.js',function(){
        $('#arquivo').fileinput({
            // inputText: 'ANEXAR CURRÍCULO',
            buttonText: 'SELECIONAR ARQUIVO'
            // buttonText: 'SELECIONAR'
        });
        $('#arquivo').change(function(e){
            if($(this).val().length)
                $('.fileinput-wrapper').addClass('has-file');
            else
                $('.fileinput-wrapper').removeClass('has-file');
        });
    });
    
    // if(APP_ENV=='development') Test.formTrabalhe();

    $('#trabalhe-conosco').click(function(e){
        $('#trabalhe-wrapper').toggleClass('open');
    });
    
    $('#trabalhe form .sexo').click(function(e){
        var $this = $(this);
        $('.sexo').removeClass('active');
        $this.addClass('active');
        $('#sexo').val($this.data('sexo'));
    });

    $('form.contato.trabalhe').submit(function(e){
        var $form = $(this),
            status = $form.find('.status');

        if(!validFormTrabalhe(this)){
            e.preventDefault();
            status.html('* Preencha os campos corretamente.');
            return false;
        }
    
        return true;
    });
});

function validFormTrabalhe(form) {
    var $form = $(form);
        inputs = $form.find('.required'),
        valid = true;

    inputs.each(function(i,elm){
        if(elm.value=='' || elm.value=='__none__')
            valid = false;
        // console.log([i,elm.value,valid]);
    });

    return valid;
}

function _prevalues(v){
    $('.prevalue').each(function(){
        var $t = $(this);
        $t.prevalue(v || $t.data('prevalue'));
    });
}

function _resetForm(formSelector){
    $(formSelector).find('input,textarea').val('');
    $(formSelector).find('select option:first-child').attr('selected',true);
}