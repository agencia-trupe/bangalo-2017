<?php

class Admin_Form_Login extends Zend_Form{
    public function init()
    {
        $this->setMethod('post')->setAction('')->setAttrib('id','flogin')->setAttrib('name','flogin');
		
		$login = $this->createElement('text','login');
		$login->setRequired(true)->setAttrib('class','txt');

		$pass = $this->createElement('password','senha');
		$pass->setRequired(true)->setAttrib('class','txt');
        
        $submit = $this->createElement('submit','submit');
		$submit->setLabel('Login')->setAttrib('class','bt');
		
		$this->addElement($login)
            ->addElement($pass)
            ->addElement($submit);
        
        $this->removeDec(array('errors'));
    }
    
    private function removeDec($decorators = array('label','htmlTag','description','errors'))
    {
        foreach($this->getElements() as $elm){
            foreach($decorators as $decorator){
                $elm->removeDecorator($decorator);
            }
        }
    }
}