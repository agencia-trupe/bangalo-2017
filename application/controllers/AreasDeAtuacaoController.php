<?php

class AreasDeAtuacaoController extends ZendPlugin_Controller_Action
{
	protected $_require_db = array(
		'table' => 'Categorias',
		'paginas' => 'Paginas',
	);

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $pagina = $this->paginas->getPagina(3);
        $this->view->pagina = $pagina;

        $areas = _utfRows($this->table->fetchAll('status_id=1','ordem'));
        $this->view->rows = $areas;
    }


}

