<?php

class IndexController extends ZendPlugin_Controller_Action
{
	protected $_require_db = array(
		'paginas' => 'Paginas',
		'categorias' => 'Categorias',
		'produtos' => 'Produtos',
		'destaques' => 'Destaques',
		'clientes' => 'Clientes2',
		'videos' => 'Videos',
	);

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $pag_empresa = _utfRow($this->paginas->get(2));
        $this->view->pag_empresa = $pag_empresa;

        $banners = $this->destaques->fetchAllWithPhoto('status_id=1','ordem');
        $this->view->banners = $banners;

        $stands = _utfRows($this->produtos->fetchAll('status_id=1','titulo'));
        $stands = $this->produtos->getFotos($stands,1);
        $this->view->stands = $stands;

        $areas = $this->categorias->fetchAllWithPhoto('status_id=1','ordem');
        $this->view->areas = $areas;

        $clientes = $this->clientes->fetchAllWithPhoto('status_id=1','ordem');
        $this->view->clientes = $clientes;

        $videos = _utfRows($this->videos->fetchAll('status_id=1','ordem',2));
        $this->view->videos = $videos;
    }

    public function mailAction()
    {
        try{
            Trupe_Bangalo_Mail::send(SITE_NAME.'@mailinator.com','Trupe','Confirmacao','Confirmar leitura do texto');
            echo "OK";
        } catch(Exception $e){
            echo $e->getMessage();
        }
        exit();
    }


}