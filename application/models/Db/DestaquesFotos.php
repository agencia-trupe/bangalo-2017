<?php

class Application_Model_Db_DestaquesFotos extends Zend_Db_Table
{
    protected $_name = "destaques_fotos";
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Destaques');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Destaques' => array(
            'columns' => 'destaque_id',
            'refTableClass' => 'Application_Model_Db_Destaques',
            'refColumns'    => 'id'
        )
    );
}
