<?php

class Application_Model_Db_Clientes2 extends ZendPlugin_Db_Table 
{
    protected $_name = "clientes2";
    protected $_foto_join_table = 'clientes2_fotos';
    protected $_foto_join_table_field = 'cliente_id';
    
    /**
     * Retorna registro por alias
     * 
     * @param array $rows - rowset para buscar, caso não seja passado busca na tabela
     */
    public function getByAlias($alias,$rows=null)
    {
        if(!$rows) return $this->fetchRow('alias = "'.$alias.'"');

        $find = null;
        
        foreach($rows as $row) if($row->alias == $alias) $find = $row;

        return $find;
    }

    /**
     * Retorna as fotos do da cliente
     *
     * @param int $id - id da cliente
     *
     * @return array - rowset com fotos da cliente
     */
    public function getFotosById($id=null)
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('clientes_fotos as tf')
            ->join('fotos as f','f.id=tf.foto_id')
            ->order('tf.id asc');
        
        if($id) $select->where('cliente_id in ('.$id.')');
        
        $fotos = $select->query()->fetchAll();
        
        array_walk($fotos,'Func::_arrayToObject');
        
        return $fotos;
    }

    /**
     * Retorna fotos dos clientes
     * 
     * @param array $clientes - rowset de clientes para associar fotos
     * @param bool  $withObjects - se irá retornar as fotos ou somente id
     * 
     * @return array of objects - clientes com fotos
     */
    public function getFotos($clientes,$withObjects=false)
    {
        $pids = array(); // ids de clientes

        // identificando clientes
        foreach($clientes as $cliente) $pids[] = $cliente->id;

        if($withObjects){ // se o retorno for com objetos
            $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
            $select->from('clientes_fotos as fc',array('fc.cliente_id'))
                   ->joinLeft('fotos as f','f.id = fc.foto_id',array('*'))
                   ->where('fc.cliente_id in ('.(count($pids) ? implode(',',$pids) : '0').')');
            
            // $fotos = $select->query()->fetchAll();
            // $fotos = count($fotos) ? array_map('Is_Array::toObject',$fotos) : array();
            $_fotos = $select->query()->fetchAll();
            $fotos = array();
            if(count($_fotos)){ foreach($_fotos as $foto){ 
                $fotos[] = Is_Array::toObject($foto);
            }}
        } else {
            $eClientes_fotos = new Application_Model_Db_ClientesFotos();
            $fotos = $eClientes_fotos->fetchAll('cliente_id in ('.(count($pids) ? implode(',',$pids) : '0').')');
        }

        // associando fotos
        foreach($clientes as &$cliente){
            $cliente->fotos = $this->getFotosSearch($cliente->id,$fotos);
        }

        return $clientes;
    }

    /**
     * Monta rowset de fotos com base no cliente_id ($pid)
     */
    public function getFotosSearch($pid=null,$cats=array())
    {
        $fotos = array();

        foreach($cats as $cat) if($cat->cliente_id == $pid) $fotos[] = $cat;
        
        return $fotos;
    }
    
}