<?php

class Application_Model_Db_MensagensContato extends ZendPlugin_Db_Table
{
    protected $_name = "mensagens_contato";
    protected $_autoUtf8 = true;

    /**
     * Retorna mensagem ref. de row
     */
    public function getMensagem($row)
    {
    	$mensagem = $this->q('select * from mensagens where id = "'.@$row->mensagem_id.'"');
    	$row->_mensagem = $mensagem ? $mensagem[0] : null;
    	return $row;
    }

    /**
     * Retorna mensagem ref. de rowset
     */
    public function getMensagens($rows)
    {
    	$mids = array(); $rids = array();
    	foreach($rows as $r) if($r->mensagem_id){
    		$mids[$r->id] = $r->mensagem_id;
    		if(!isset($rids[@$r->mensagem_id])) $rids[$r->mensagem_id] = array();
    		$rids[$r->mensagem_id][] = $r->id;
    	}
    	
    	$mensagens = $this->q('select * from mensagens where id in ('.implode(',',array_unique($mids)).')');
    	
    	foreach($rows as $r) foreach($mensagens as $m)
			if(in_array($r->id, $rids[$m->id])) $r->_mensagem = $m;
    	
    	return $rows;
    }

    /**
     * Retorna arquivo ref. de row
     */
    public function getArquivo($row)
    {
    	$arquivo = $this->q('select * from arquivos where id = "'.@$row->arquivo_id.'"');
    	$row->_arquivo = $arquivo ? $arquivo[0] : null;
    	return $row;
    }

    /**
     * Retorna arquivo ref. de rowset
     */
    public function getArquivos($rows)
    {
    	$aids = array(); $rids = array();
    	foreach($rows as $r) if($r->arquivo_id){
    		$aids[$r->id] = $r->arquivo_id;
    		if(!isset($rids[$r->arquivo_id])) $rids[$arquivo_id] = array();
    		$rids[$r->arquivo_id][] = $r->id;
    	}
    	
    	$arquivos = $this->q('select * from arquivos where id in ('.implode(',',array_unique($aids)).')');
    	
    	foreach($rows as $r) foreach($arquivos as $a)
			if(in_array($r->id, $rids[$a->id])) $r->_arquivo = $a;
    	
    	return $rows;
    }

}
