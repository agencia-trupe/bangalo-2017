<?php

class Application_Model_Db_Categorias extends ZendPlugin_Db_Table
{
    protected $_name = "categorias";
    protected $default_order = 'ordem'; // ordem padrão para ordenação dos registros
    protected $_foto_join_table = null; // tabela de relação para registros de fotos
    protected $_foto_join_table_field = null; // campo de relação para registros da tabela principal
    protected $_foto_join_field = 'foto_id'; // campo para dar join com foto.id
    
    /**
     * Referências
     */
    protected $_dependentTables = array('Application_Model_Db_Produtos');
    
    protected $_referenceMap = array(
        'Application_Model_Db_Produtos' => array(
            'columns' => 'id',
            'refTableClass' => 'Application_Model_Db_Produtos',
            'refColumns'    => 'categoria_id'
        )
    );
    
    /**
     * Retorna chave => valor ('id'=>valor)
     *
     * @param string $text  - campo a ser retornado como valor - padrão('descrição')
     * @param bool   $combo - se for true, adiciona um valor 'selecione...' para ser usado como combobox
     *
     * @return array
     */
    public function getKeyValues($text='descricao',$combo=false)
    {
        $values = $combo ? array("__none__"=>"Selecione...") : array();
        $rows = $this->fetchAll(null,'ordem');
        
        foreach($rows as $row){
            $values[$row->id] = utf8_encode($row->$text);
        }
        
        return $values;
    }
    
    /**
     * Retorna categoria com seus produtos e imagens com base no alias se @alias for string ou id se @alias for numérico
     *
     * @param string|int $alias  - valor do alias ou id da categoria
     * @param int        $limit  - limite do select - default null
     * @param int        $offset - offset do select - default null
     * @param bool       $ativos - retorna somente produtos ativos?
     *
     * @return object|bool - objeto contendo a categoria com produtos ou false se não for encontrado
     */
    public function getWithProdutos($alias,$limit=null,$offset=null,$ativos=false,$order='id desc',$where=null)
    {
        $column = is_numeric($alias) ? 'id' : 'alias';
        if(!$categoria = $this->fetchRow($column.'="'.$alias.'"')){
            return false;
        }
        $fotos = array();

        // selecionando relações da categoria com produtos
        $_pc = new Application_Model_Db_ProdutosCategorias();
        $produto_categs = $_pc->fetchAll('categoria_id='.$categoria->id);
        $produto_ids = array();
        foreach($produto_categs as $pc) $produto_ids[] = $pc->produto_id;
        $produto_ids = count($produto_ids) ? implode(',',$produto_ids) : '0';
        
        //if($produtos = $categoria->findDependentRowset('Application_Model_Db_Produtos')){
        $produtos_table = new Application_Model_Db_Produtos();
        $produtos_order = $order == 'cor_id' ? null : $order;
        // $produtos_where = 'categoria_id="'.$categoria->id.'"'.($ativos?' and status_id = 1':'').($where?' and '.$where:'');
        $produtos_where = 'id in ('.$produto_ids.')'.($ativos?' and status_id = 1':'').($where?' and '.$where:'');
        
        if($produtos = $produtos_table->fetchAll($produtos_where,$produtos_order,$limit,$offset)){
            foreach($produtos as $produto){            
                $fotos[$produto->id] = array();
                $select_fotos = $this->select()->order($order=='cor_id'?'cor_id':'id desc');
                
                if($produto_fotos = $produto->findDependentRowset('Application_Model_Db_ProdutosFotos',null,$select_fotos)){
                    foreach($produto_fotos as $produto_foto){
                        $fotos[$produto->id] = $produto_foto->findDependentRowset('Application_Model_Db_Fotos');
                    }
                }
            }
        }
        
        $object = Is_Array::utf8DbRow($categoria);
        //$object->produtos = $produtos ? Is_Array::utf8DbResult($produtos) : null;
        $object->produtos = (bool)$produtos ? $produtos_table->checkDesconto(Is_Array::utf8DbResult($produtos)) : null;
        
        if(count($fotos)){
            foreach($object->produtos as &$produto){
                $produto->fotos = $fotos[$produto->id];
            }
        }
        
        $object->count = $limit ? $this->count_getWithProdutos($object->id,$ativos) : count($object->produtos);
        
        return $object;
    }
    
    /**
     * Retorna total de produtos na categoria 
     */
    public function count_getWithProdutos($categoria_id,$ativos=false)
    {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select->from('produtos as p',array(
                new Zend_Db_Expr('COUNT(*) as count')
            ))
            ->where('pc.categoria_id = ?',$categoria_id)
            ->joinRight('produtos_categorias as pc','pc.produto_id = p.id',array());
        
        if($ativos){ $select->where('p.status_id = 1'); }
        // _d($select->query()->__toString());
        $count = $select->query()->fetchAll();
        return $count[0]['count'];
    }
    
    /**
     * Retorna as fotos da categoira
     *
     * @param int $id - id da categoria
     *
     * @return array - rowset com fotos da categoria
     */
    public function getFotos($id)
    {
        if(!$promocao = $this->fetchRow('id="'.$id.'"')){
            return false;
        }
        $fotos = array();
        
        if($promocao_fotos = $promocao->findDependentRowset('Application_Model_Db_Fotos')){
            foreach($promocao_fotos as $promocao_foto){
                $fotos[] = Is_Array::utf8DbRow($promocao_foto->current());
            }
        }
        
        return $fotos;
    }
    
    /**
     * Retorna categorias pai
     *
     * @param mixed $categ - array  : lista de resultados para filtrar e retornar somente pais
     *                       object : objeto resultado para filtrar e retornar sua categoria pai
     *                       int    : id da subcategoria p/ retornar sua categoria pai
     *                       null   : retorna todas as categorias pai do db
     *                       
     * @param bool $parent_null - opcional - usar somente se for passado um rowset(array) como 1º argumento
     *                            false : retornará os pais dos registros com parent_id (padrão)
     *                            true  : retornará todos os pais do rowset
     * 
     * @return array - rowset com categorias pai
     */
    public function getParents($categ=null,$parent_null=false)
    {
        if(is_array($categ)){ // filtra resultSet
            $cats = array();
            
            foreach($categ as $cat){
                if($parent_null){
                    if(!(bool)$cat->categoria_id)
                        $cats[] = $cat;
                } else {
                    if((bool)$cat->categoria_id)
                        $cats[] = $this->checkParentFromRowset($cat->categoria_id,$categ);
                }
            }
        } else if(is_object($categ)){ // busca pai de row
            return (bool)$categ->categoria_id ? $this->fetchRow('id = '.$categ->categoria_id) : null;
        } else if(is_int($categ)){ // busca por id
            $row = $this->fetchRow('id = '.$categ);
            return (bool)$row->categoria_id ? $this->fetchRow('id = '.$row->categoria_id) : null;
        } else { // busca todos os pais
            $cats = $this->fetchAll('categoria_id is null');
        }
        
        return $cats;
    }
    
    /**
     * Checa qual item do resultset é pai
     *
     * @param int   $cat  - categoria_id (id do pai a procurar)
     * @param array $cats - resultset p/ filtrar pai
     */
    public function checkParentFromRowset($cat,$cats)
    {
        foreach($cats as $c){
            if($c->id == $cat){
                return $c;
            }
        }
    }
    
    /**
     * Retorna key/values dos pais
     *
     * @param array $cats - resultset de categorias_pai
     * @param array $first - primeiro(s) elementos do array retornado - Ex.: 0 => 'Selecione...'
     */
    public function getParentsKV($cats,$first=null)
    {
        $keyvalues = is_array($first) ? $first : array();
        foreach($cats as $cat) $keyvalues[$cat->id] = $cat->descricao;
        return $keyvalues;
    }
    
    /**
     * Retorna categorias filhas
     *
     * @param int $parent_id - id da categoria pai
     */
    public function getChildren($parent_id)
    {
        return $this->fetchAll('categoria_id = '.$parent_id);
    }
    
    /**
     * Retorna categorias com filhos
     *
     * @param string $where - where do select (padrão: categoria_id is null)
     * @param string $order - order do select (padrão: ordem)
     * @param string $ident - identação das subcategorias (padrão; - )
     */
    public function getWithChildren($where='categoria_id is null',$order='ordem',$ident='- ')
    {
        $_cats = $this->fetchAll($where,$order);
		$_cats = count($_cats) ? Is_Array::utf8DbResult($_cats) : $cats;
		$cats = array();
		
        foreach($_cats as $_cat){
			$cats[] = $_cat;
			$children = $this->getChildren($_cat->id);
			if(count($children)){
				$children = Is_Array::utf8DbResult($children);
				foreach($children as &$child){
					$child->descricao = $ident.$child->descricao;
					$cats[] = $child;
				}
			}
		}
        
        return $cats;
    }
    
    /**
     * Retorna produtos de categorias em promoção
     */
    public function getProductsInPromo($limit=null)
    {
        $this->produtos = new Application_Model_Db_Produtos();
        $promos = $this->fetchAll('desconto is not null and desconto <> "0" and id <> '.VALEPRESENTEID,null,$limit);
        $produtos = array();
        
        if(count($promos)){
            $promos = Is_Array::utf8DbResult($promos);
            
            foreach($promos as &$promo){
                $prods = $this->getWithProdutos($promo->alias,$limit,null,true);
                if(count($prods->produtos)){
                    foreach($prods->produtos as $p) array_push($produtos,$p);
                }
            }
        }
        
        return $produtos;
    }

    /**
     * Retorna itens para montar menu
     */
    public function getMenu($menu)
    {
        $categs = $this;

        // adicionando tamanhos ao menu
        $config_tamanho = array();
        $tamanhos_rows = $this->q('select * from tamanhos group by alias order by ordem, descricao');
        $i = 0;
        
        foreach($tamanhos_rows as $tamanho){
            $config_tamanho[$tamanho->alias] = array(
                'label'     => utf8_encode($tamanho->descricao),
                'title'     => utf8_encode($tamanho->descricao),
                'descricao' => utf8_encode($tamanho->descricao),
                'uri'       => URL.'/e-store/tamanho/'.$tamanho->alias,
                'id'        => 'tamanho-'.$tamanho->id,
                'class'     => $tamanho->alias
            );
            
            $i++;
        }

        // adicionando categorias ao menu
        $config_categ = array();
        $categs_rows = $categs->fetchAll('status_id = 1 and categoria_id is null','ordem');
        $i = 0;
        
        foreach($categs_rows as $categ){
            $config_categ[$categ->alias] = array(
                'label'     => utf8_encode($categ->descricao),
                'title'     => utf8_encode($categ->descricao),
                'descricao' => utf8_encode($categ->body),
                'uri'       => URL.'/e-store/tipo/'.$categ->alias,
                'id'        => 'categoria-'.$categ->id,
                'class'     => $categ->alias
            );
            
            $i++;
        }

        // adicionando classificacao ao menu
        $config_classif = array();
        $config_classif['mais-recentes'] = array(
            'label'     => 'Mais recentes',
            'title'     => 'Mais recentes',
            'descricao' => 'Mais recentes',
            'uri'       => URL.'/e-store/mais-recentes',
            'id'        => 'id-desc',
            'class'     => 'id-desc'
        );
        $config_classif['menor-valor'] = array(
            'label'     => 'Menor valor',
            'title'     => 'Menor valor',
            'descricao' => 'Menor valor',
            'uri'       => URL.'/e-store/menor-valor',
            'id'        => 'valor-asc',
            'class'     => 'valor-asc'
        );
        $config_classif['maior-valor'] = array(
            'label'     => 'Maior valor',
            'title'     => 'Maior valor',
            'descricao' => 'Maior valor',
            'uri'       => URL.'/e-store/maior-valor',
            'id'        => 'valor-desc',
            'class'     => 'valor-desc'
        );

        // adicionando cores ao menu
        $config_cor = array();
        $cors_rows = $this->q('select * from cores group by alias order by ordem');
        $i = 0;
        
        foreach($cors_rows as $cor){
            $config_cor[$cor->alias] = array(
                'label'     => utf8_encode($cor->descricao),
                'title'     => utf8_encode($cor->descricao),
                'descricao' => utf8_encode($cor->descricao),
                'uri'       => URL.'/e-store/cor/'.$cor->alias,
                'id'        => 'cor-'.$cor->id,
                'class'     => $cor->alias
            );
            
            $i++;
        }
        
        // montando arrays
        $menu['tamanho']['pages'] = $config_tamanho;
        $menu['tipo']['pages'] = $config_categ;
        $menu['classificar']['pages'] = $config_classif;
        $menu['cor']['pages'] = $config_cor;
        
        return $menu;
    }

    public function getMenuOld($withSubs=true)
    {
        $categs = $this;
        $config_categ = array();

        // seleciona todas as categorias ativas
        // $categs_rows = $categs->fetchAll('status_id = 1 and categoria_id is null','descricao');
        
        // seleciona categorias ativas somente que tiverem curso cadastrado
        $categs_rows = $categs->q(
            'select c.id, c.descricao, c.alias, c.body '.
                ', count(t.id) as cnt_t '.
            'from categorias c '.
                'left join treinamentos t on t.categoria_id = c.id '.
            'where 1=1 '.
                'and c.status_id = 1 and c.categoria_id is null '.
            'group by c.id '.
            'having cnt_t > 0 '.
            'order by c.ordem, c.descricao'
        );
        
        $i = 0;
        foreach($categs_rows as $categ){
            $config_categ[$categ->alias] = array(
                'label'     => ($categ->descricao),
                'title'     => ($categ->descricao),
                'descricao' => ($categ->body),
                //'uri'     => (APPLICATION_ENV == 'development'?'/'.SITE_NAME.'/':'/').'categoria/'.$categ->alias.'/'
                // 'uri'       => URL.'/categoria/'.$categ->alias.'/',
                'uri'       => URL.'/treinamentos/area/'.$categ->alias.'',
                // 'uri'       => URL.'/treinamentos/treinamentos-abertos/'.$categ->alias.'-'.$categ->id,
                'id'        => 'categoria-'.$categ->id,
                'class'     => $categ->alias
            );
            
            if($withSubs){ // adiciona subcategorias
                $children = $categs->getChildren($categ->id);
                if(count($children)){
                    $config_categ[$categ->alias]['pages'] = array();

                    /*$config_categ[$categ->alias]['pages'][] = array(
                        'label'     => ($categ->descricao),
                        'title'     => ($categ->descricao),
                        'descricao' => ($categ->body),
                        'uri'       => URL.'/categoria/'.$categ->alias.'/',
                        'class'     => 'submenu-title'
                    );*/
                    
                    foreach($children as $child){
                        $config_categ[$categ->alias]['pages'][] = array(
                            'label'     => ($child->descricao),
                            'title'     => ($categ->descricao),
                            'descricao' => ($categ->body),
                            //'uri'     => (APPLICATION_ENV == 'development'?'/'.SITE_NAME.'/':'/').'categoria/'.$categ->alias.'/'
                            'uri'       => URL.'/categoria/'.$child->alias.'/',
                            'class'     => 'submenu submenu-'.$categ->alias
                        );
                    }
                }
            }
            
            $i++;
            /*if($i >= sizeof($categs_rows)){
                $config_categ[$categ->alias]['class'] = 'last';
                
                $config_categ['vale-presente'] = array(
                    'label' => 'vale presente',
                    'uri'   => URL.'/categoria/vale-presente/',
                    'class' => 'vale-presente last'
                );
            }*/
        }
        
        // _d($config_categ);
        return $config_categ;
    }
}